//
//  BookMarksViewController.swift
//  StyleO
//
//  Created by Dinesh Kumar on 06/12/15.
//  Copyright © 2015 Dinesh Kumar. All rights reserved.
//

import UIKit

class BookMarksViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - IBOUTLETS
    @IBOutlet weak var noBookmarksPlaceholder: UILabel!
    @IBOutlet weak var bookMarksTableView: UITableView!
    
    //MARK: - INITIALISE VARIABLES
    var bookMarksArray : NSArray!
    let backgroundQueue = dispatch_queue_create("asdasd", nil)
    
    //MARK: - VIEW APPEAR LOAD FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bookMarksArray = NSArray()
        bookMarksTableView.registerNib(UINib(nibName: "bookMarkTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "bookMarkCellIdentifier")
        readBookMarkTextFileFromDocumentDirectory()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden=false
        self.navigationItem.title = "Bookmarks"
    }
    
    //MARK: - READ USER BOOKMARKS FROM TEXT FILE
    func readBookMarkTextFileFromDocumentDirectory(){
        let documentUrl = getDocumentsURL()
        var dataPath = "\(documentUrl)".stringByAppendingString("/\(NSUserDefaults.standardUserDefaults().valueForKey(loggedInUserFacebookId) as! String)")
        dataPath = dataPath.stringByAppendingString("/Bookmark.txt")
        print(dataPath)
        do {
            let text2 = try String(contentsOfFile: dataPath)
            if(text2 == "(\n)"){
                noBookmarksPlaceholder.hidden = false
            }else{
                noBookmarksPlaceholder.hidden = true
                
                bookMarksArray = text2.componentsSeparatedByString("\n")
                print(bookMarksArray)
                bookMarksTableView.reloadData()
            }
        }
        catch let error as NSError {
            noBookmarksPlaceholder.hidden = false
            print(error.localizedDescription);
        }
    }
    
    //MARK: - TABLEVIEW DELEGATES
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookMarksArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let bookMarkCell = tableView.dequeueReusableCellWithIdentifier("bookMarkCellIdentifier", forIndexPath: indexPath) as! BookmarkTableViewCell
        bookMarkCell.shareButton.hidden = false
        
        dispatch_async(backgroundQueue, {
            let combinedUrl = self.bookMarksArray.objectAtIndex(indexPath.row) as! String
            let arrayOfUrls = combinedUrl.componentsSeparatedByString("$#$") as NSArray
            let tShirtImage = UIImage(contentsOfFile: currentUserTShirtFolder() + "/" + (arrayOfUrls.objectAtIndex(0) as! String) )
            let pantImage = UIImage(contentsOfFile: currentUserPantFolder() + "/" + (arrayOfUrls.objectAtIndex(1) as! String))
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                bookMarkCell.userTShirtImageView.image = tShirtImage
                bookMarkCell.userPantImageView.image = pantImage
            })
        })
        bookMarkCell.shareButton.titleLabel?.numberOfLines = 1;
        bookMarkCell.shareButton.setTitle("  Share\n\(indexPath.row)", forState: UIControlState.Normal)
        bookMarkCell.shareButton.addTarget(self, action: "shareAction:", forControlEvents: UIControlEvents.TouchUpInside)
        bookMarkCell.shareScreenShotView.backgroundColor = UIColor.clearColor()
        
        return bookMarkCell
    }
    
    //MARK: - SHARE BUTTON PRESSED
    func shareAction(sender:UIButton){
        let tempArr = (sender.currentTitle!).componentsSeparatedByString(" Share\n") as NSArray
        let currentIndex = Int(tempArr.objectAtIndex(1) as! String)
        let bookmarkCell = bookMarksTableView.cellForRowAtIndexPath(NSIndexPath(forRow: currentIndex!, inSection: 0)) as! BookmarkTableViewCell
        
        //OPENING UIACTIVITY FOR SHARING IMAGE
        let screenShotImage: UIImage = takeSnapshot(bookmarkCell.shareScreenShotView)
        let shareItems:Array = [screenShotImage]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivityTypePrint, UIActivityTypePostToWeibo, UIActivityTypeCopyToPasteboard, UIActivityTypeAddToReadingList, UIActivityTypePostToVimeo]
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    //MARK: - TAKE SCREEN SHOT FUNCTION
    func takeSnapshot(yourView:UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(yourView.bounds.size, false, UIScreen.mainScreen().scale)
        yourView.drawViewHierarchyInRect(yourView.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    //MARK: - LOGOUT BUTTON PRESSED
    @IBAction func logoutPressed(sender: UIButton) {
        
        let alert = UIAlertController(title: "", message: "Are you sure you want to log out?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { action in
            let appDomain = NSBundle.mainBundle().bundleIdentifier!
            NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
            
            let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appdelegate.logoutFunction()        }))
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
