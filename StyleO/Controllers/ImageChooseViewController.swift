//
//  ImageChooseViewController.swift
//  StyleO
//
//  Created by Dinesh Kumar on 06/12/15.
//  Copyright © 2015 Dinesh Kumar. All rights reserved.

import UIKit

class ImageChooseViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource {
    
    //MARK: - IBOUTLETS
    @IBOutlet weak var pantCollectionView: UICollectionView!
    @IBOutlet weak var tShirtCollectionView: UICollectionView!
    
    //MARK: - INITIALISE VARIABLES
    var isTShirtButtonPressed: Bool!
    
    //MARK: - VIEW APPEAR LOAD FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pantCollectionView.registerNib(UINib(nibName: "selectedImage", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "selectedCellIdenitifier")
        
        tShirtCollectionView.registerNib(UINib(nibName: "selectedImage", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "selectedCellIdenitifier")
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden=false
        
        self.navigationItem.title = "StyleO"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Try Out!", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("tryOutPressed:"))
    }
    
    //MARK: - COLLECTION VIEW DELEGATES & DATASOURCE
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == tShirtCollectionView {
            return currentUserTotalTShirts().count + 1
        }
        else{
            return currentUserTotalPants().count + 1
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width:(UIScreen.mainScreen().bounds.height-140)/2, height: (UIScreen.mainScreen().bounds.height-140)/2);
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
            return 0
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
            return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let selectedCell = collectionView.dequeueReusableCellWithReuseIdentifier("selectedCellIdenitifier", forIndexPath: indexPath) as! SelectedImageCell
        var imagePath : String!
        var selectedImage : UIImage!
        
        
        if collectionView == tShirtCollectionView {
            let imageNames = currentUserTotalTShirts()
            
            if(indexPath.row == imageNames.count){
                selectedImage = UIImage(named: "btn_add_image")
                
            }else{
                
                imagePath = imageNames[indexPath.row]
                imagePath = currentUserTShirtFolder() + "/" + imagePath
                NSLog("imagePath%@", imagePath)
                selectedImage = UIImage(contentsOfFile: imagePath)
            }
        }else{
            let imageNames = currentUserTotalPants()
            if(indexPath.row == imageNames.count){
                selectedImage = UIImage(named: "btn_add_image")
            }else{
                imagePath = imageNames[indexPath.row]
                
                imagePath = currentUserPantFolder() + "/" + imagePath
                NSLog("imagePath%@", imagePath)
                
                selectedImage = UIImage(contentsOfFile: imagePath)
            }
        }
        
        selectedCell.selectedItemImageView.image = selectedImage
        selectedCell.backgroundColor = UIColor.clearColor()
        return selectedCell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if collectionView == tShirtCollectionView {
            let imageNames = currentUserTotalTShirts()
            if(indexPath.row == imageNames.count){
                isTShirtButtonPressed = true
                imageSelectionMethod()
            }
        }
        else{
            let imageNames = currentUserTotalPants()
            if(indexPath.row == imageNames.count){
                
                isTShirtButtonPressed = false
                imageSelectionMethod()
            }
        }
    }
    
    //MARK: - OPEN CAMERA FOR IMAGE SELECTION
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            let imagePicker = UIImagePickerController()
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            imagePicker.sourceType = .Camera
            presentViewController(imagePicker, animated: true, completion: nil)
        } else{
            Alert(self,alertMessage: "Camera not available!")
        }
    }
    
    //MARK: - OPEN GALLERY FOR IMAGE SELECTION
    func  openGallery () {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        imagePicker.sourceType = .PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    
    //MARK: - TRY OUT BUTTON PRESSED
    @IBAction func tryOutPressed(sender: UIButton) {
        
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appdelegate.openWearSuggestionView()
    }
    
    //MARK: - OPEN ACTION SHEET FOR IMAGE SELECTION
    func imageSelectionMethod(){
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        let GalleryAction: UIAlertAction = UIAlertAction(title: "Gallery", style: .Default) { action -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
            self.openGallery()
        }
        
        let CameraAction: UIAlertAction = UIAlertAction(title: "Camera", style: .Default ) { action -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
            self.openCamera()
        }
        let CancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel ) { action -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        alert.addAction(GalleryAction)
        alert.addAction(CameraAction)
        alert.addAction(CancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    // MARK: - IMAGE PICKER CANCEL DELEGATE
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - IMAGE SELECTION FINISH DELEGATE
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            let currentTimeStamp = "\(NSDate())"
            let loginUserId = NSUserDefaults.standardUserDefaults().valueForKey(loggedInUserFacebookId) as! String
            if(isTShirtButtonPressed == true){
                let tShirtPath = getDocumentsURL().stringByAppendingString("/\(loginUserId)/TShirts/\(currentTimeStamp).jpeg")
                print("tShirtPath : \(tShirtPath)")
                let jpegData = UIImageJPEGRepresentation(pickedImage, 0.1)
                
                jpegData!.writeToFile(tShirtPath, atomically: true)
                tShirtCollectionView.reloadData()
            }
            else{
                let pantPath = getDocumentsURL().stringByAppendingString("/\(loginUserId)/Pants/\(currentTimeStamp).jpeg")
                print("pantPath : \(pantPath)")
                let jpegData = UIImageJPEGRepresentation(pickedImage, 0.6)
                jpegData!.writeToFile(pantPath, atomically: true)
                pantCollectionView.reloadData()
            }
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
