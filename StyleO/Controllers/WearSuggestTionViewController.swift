//
//  WearSuggestTionViewController.swift
//  StyleO
//
//  Created by Dinesh Kumar on 07/12/15.
//  Copyright © 2015 Dinesh Kumar. All rights reserved.
//

import UIKit

class WearSuggestTionViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    //MARK: - IBOUTLETS
    @IBOutlet weak var wearViewCollectionView: UICollectionView!
    @IBOutlet weak var addMoreLabel : UILabel!
    
    //MARK: - INITIALISE VARIABLES
    var totalCollection : [Dictionary<String, AnyObject>] = []
    var isThisViewHomeScreen:Bool!
    
    //MARK: - VIEW APPEAR LOAD FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        creatingCombinationSelected(currentUserTotalTShirts(), pants: currentUserTotalPants())
        totalCollection = totalCasesArray
        wearViewCollectionView.registerNib(UINib(nibName: "wearThisStyleView", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "wearCollectionView")
        
        if(totalCollection.count == 0){
            addMoreLabel.hidden = false
        }
        else{
            addMoreLabel.hidden = true
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden=false
        self.navigationItem.title = "StyleO"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "My Bookmarks", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("bookMarksPressed:"))
        
        if (self.isThisViewHomeScreen == true){
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "+ Add", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("addMoreAction:"))
        }
    }
    
    //MARK: - MY BOOKMARK BUTTON PRESSED
    func bookMarksPressed(sender:UIButton){
        var bookMarkViewObject : BookMarksViewController!
        bookMarkViewObject = self.storyboard?.instantiateViewControllerWithIdentifier("bookMarksView") as! BookMarksViewController
        self.navigationController?.pushViewController(bookMarkViewObject, animated: true)
        
    }
    
    //MARK: - COLLECTION VIEW DELEGATES & DATA SOURCE
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return totalCollection.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width:UIScreen.mainScreen().bounds.width, height: UIScreen.mainScreen().bounds.height-70);
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
            return 0
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
            return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let todayWearCell = collectionView.dequeueReusableCellWithReuseIdentifier("wearCollectionView", forIndexPath: indexPath) as! WearableTableCell
        
        let wearCombination = totalCollection[indexPath.row]
        var newEntry =  Dictionary<String,AnyObject>()
        newEntry = wearCombination
        
        //ASSIGN T SHIRT IMAGE FROM DOCUMENT PATH
        let tShirtUrl:String = newEntry["tShirturl"] as! String
        let tShirtImage = UIImage(contentsOfFile: currentUserTShirtFolder() + "/" + tShirtUrl)
        todayWearCell.tshirtImageView.image = tShirtImage
        
        //ASSIGN PANT IMAGE FROM DOCUMENT PATH
        let pantUrl:String = newEntry["pantUrl"] as! String
        let pantImage = UIImage(contentsOfFile: currentUserPantFolder() + "/" + pantUrl)
        todayWearCell.pantImageView.image = pantImage
        
        //SETTING DISLIKE & BOOKMARK BUTTON TITLE WITH INDEXPATH FOR FURTHER ACTIONS
        todayWearCell.dislikeButton.exclusiveTouch = true
        todayWearCell.dislikeButton.titleLabel?.numberOfLines = 1;
        todayWearCell.dislikeButton.setTitle("\n\(indexPath.row)", forState: UIControlState.Normal)
        todayWearCell.dislikeButton.addTarget(self, action: "dislikeButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        todayWearCell.bookmarkButton.exclusiveTouch = true
        todayWearCell.bookmarkButton.titleLabel?.numberOfLines = 1;
        todayWearCell.bookmarkButton.setTitle("\n\(indexPath.row)", forState: UIControlState.Normal)
        todayWearCell.bookmarkButton.addTarget(self, action: "bookmarksPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        todayWearCell.backgroundColor = UIColor.clearColor()
        return todayWearCell
    }
    
    //MARK: - DISLIKE BUTTON PRESSED
    func dislikeButtonPressed(sender:UIButton){
        
        let tempArr = (sender.currentTitle!).componentsSeparatedByString("\n") as NSArray
        let currentIndex = Int(tempArr.objectAtIndex(1) as! String)
        openNextStyleCombination(currentIndex!)
    }
    
    //MARK: - DELETE INDEX & UPDATE VIEW
    
    func openNextStyleCombination(currentIndex: Int){
        
        totalCollection.removeAtIndex(currentIndex)
        wearViewCollectionView.reloadData()
        scrollToParticularIndex()
    }
    
    //MARK: - SCROLL TO RANDOM INDEX AFTER DISLIKE
    func scrollToParticularIndex(){
        if(totalCollection.count>1){
            let randomIndex = Int(arc4random_uniform(UInt32(totalCollection.count)-1))
            wearViewCollectionView.scrollToItemAtIndexPath(NSIndexPath(forRow: randomIndex, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: false)
        }else{
            if (totalCollection.count == 0){
            addMoreLabel.hidden = false
            }else{
                addMoreLabel.hidden = true
            }
            wearViewCollectionView.reloadData()
        }
    }
    
    //MARK: - ADD TO BOOKMARK BUTTON PRESSED
    func bookmarksPressed(sender:UIButton){
        
        let tempArr = (sender.currentTitle!).componentsSeparatedByString("\n") as NSArray
        let currentIndex = Int(tempArr.objectAtIndex(1) as! String)
        let wearCombination = totalCollection[currentIndex!]
        var newEntry =  Dictionary<String,AnyObject>()
        newEntry = wearCombination
        let tShirtUrl:String = newEntry["tShirturl"] as! String
        let pantUrl:String = newEntry["pantUrl"] as! String
        print(userBookMarksArray)
        let finalUrl = tShirtUrl + "$#$" + pantUrl
        if(!userBookMarksArray.containsObject(finalUrl)){
            userBookMarksArray.addObject(finalUrl)
            let dataPath = getDocumentsURL().stringByAppendingString("/\(NSUserDefaults.standardUserDefaults().valueForKey(loggedInUserFacebookId) as! String)")
            print(dataPath)
            var tempArray = NSArray()
            tempArray = userBookMarksArray
            let text = tempArray.componentsJoinedByString("\n")
            do {
                openNextStyleCombination(currentIndex!)
                Alert(self, alertMessage: "You have bookmarked this style combination.")
                try text.writeToFile("\(dataPath)/Bookmark.txt", atomically: false, encoding: NSUTF8StringEncoding)
            }
            catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        else
        {
            Alert(self, alertMessage: "This style combination already exists in your bookmarks.")
            
        }
    }
    
    //MARK: - ADD MORE BUTTON ACTION
    func addMoreAction(sender: UIButton) {
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appdelegate.openImageOptionView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
