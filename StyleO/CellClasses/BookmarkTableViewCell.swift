//
//  BookmarkTableViewCell.swift
//  StyleO
//
//  Created by Dinesh Kumar on 06/12/15.
//  Copyright © 2015 Dinesh Kumar. All rights reserved.
//

import UIKit

class BookmarkTableViewCell: UITableViewCell {
    @IBOutlet weak var shareScreenShotView: UIView!
    @IBOutlet weak var userTShirtImageView: UIImageView!
    @IBOutlet weak var userPantImageView: UIImageView!
    @IBOutlet weak var shareButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
